FROM ubuntu:jammy

COPY lib_gdal.tar.xz lib_gdal.tar.xz

RUN set -ex; \
    apt-get update; \
    apt-get install -y curl gnupg lsb-release; \
    echo "deb https://rm.nextgis.com/api/repo/11/deb $(lsb_release -cs) main" | tee -a /etc/apt/sources.list; \
    curl -s -L https://rm.nextgis.com/api/repo/11/deb/key.gpg | apt-key add -; \
    apt-get update;

RUN set -ex; \
    apt install -y build-essential cmake cdbs zlib1g-dev libpng-dev libjpeg-dev d-shlibs  libgeos-dev dh-python python3-all-dev python3-numpy python3-all-dev  python3-numpy libcurl4-openssl-dev chrpath swig libexpat1-dev libproj-dev libxml2-dev liblzma-dev libarmadillo-dev libtiff-dev libgeotiff-dev libjson-c-dev libsqlite3-dev libpcre3-dev libspatialite-dev libpq-dev postgresql-server-dev-all libhdf4-dev  libopencad-dev libgif-dev libqhull-dev libwebp-dev lsb-release libclntsh-dev libkml-dev libboost-dev  python3-pytest python3-lxml git-core python3-setuptools;

RUN tar -xf lib_gdal.tar.xz

RUN set -ex; \
    cd lib_gdal && mkdir build && cd build; \
    cmake -DBUILD_SHARED_LIBS=ON -DBUILD_STATIC_LIBS=ON -DWITH_EXPAT=ON -DWITH_GeoTIFF=ON -DWITH_ICONV=ON -DWITH_JSONC=ON -DWITH_LibXml2=ON -DWITH_GEOS=ON -DWITH_PROJ=ON -DWITH_TIFF=ON -DWITH_ZLIB=ON -DWITH_JBIG=ON -DWITH_JPEG=ON -DWITH_LibLZMA=ON -DWITH_Armadillo=ON -DCMAKE_BUILD_TYPE=Release -DGDAL_INSTALL_DATA_IN_VERSION_DIR=ON -DWITH_PYTHON=ON -DWITH_PYTHON2=ON -DWITH_PYTHON3=ON -DWITH_SQLite3=ON -DWITH_CURL=ON -DWITH_PNG=ON -DENABLE_JPEG=ON -DENABLE_OZI=ON -DENABLE_NITF_RPFTOC_ECRGTOC=ON -DENABLE_PG=ON -DENABLE_POSTGISRASTER=ON -DENABLE_HDF4=ON -DWITH_PostgreSQL=ON -DWITH_HDF4=ON -DGDAL_ENABLE_GNM=ON -DWITH_QHULL=ON -DWITH_OCI=ON -DENABLE_OCI=ON -DENABLE_GEORASTER=ON -DWITH_KML=ON -DBUILD_TESTING=ON -DSKIP_PYTHON_TESTS=OFF ..; \
    cat /lib_gdal/build/CMakeFiles/CMakeError.log; \
    cmake --build . -- -j $(nproc); \
    cmake --build . --target install;

RUN ldconfig
